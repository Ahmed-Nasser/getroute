package ahmednasser.scriba.autosar;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;

/**
 * Created by ahmednasser on 3/16/18.
 */

public class FetchUrl extends AsyncTask<String, Void, String> {
    private GoogleMap map;
    private Context con;

    public FetchUrl(GoogleMap x,Context c) {
        map=x;
        con=c;
    }

    @Override
    protected String doInBackground(String... url) {
        Log.v("nasser","doinbackfetch");
        String data = "";

        try {
            DownloadJsonData c = new DownloadJsonData();
            data = c.downloadUrl(url[0]);
        } catch (Exception e) {
            Log.d("Background Task failed", e.toString());
        }
        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        Log.v("nasser","onpostexecutefetch");
        super.onPostExecute(result);
        ParserTask c = new ParserTask(map,con);
        c.execute(result);
    }
}
