package ahmednasser.scriba.autosar;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ahmednasser on 3/16/18.
 */

public class MakeQuery {
    private LatLng source ;
    private LatLng dest;

    public MakeQuery(LatLng x , LatLng y) {
        source=x;
        dest=y;
    }

    public String makeQuery (){
        Log.v("nasser","geturl");
        String origin = "origin=" + source.latitude + "," + source.longitude;
        String destination = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        //TODO : CHANGE API KEY HERE ALSO
        String parameters = origin + "&" + destination + "&" + sensor+"&key=AIzaSyAu4Byg5glwWLC64KclIDezomvvIs5eKRg";
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }
}
