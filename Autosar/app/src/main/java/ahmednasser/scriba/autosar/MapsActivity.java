package ahmednasser.scriba.autosar;


import androidx.fragment.app.FragmentActivity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,LocationListener {

    private GoogleMap mMap;
    private LatLng start;
    private LatLng dist;
    private Location current;
    LocationManager locationManager;
    ArrayList<MarkerOptions> arr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getLocation();
        Button b = (Button) findViewById(R.id.btn);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO CHANGE PACKAGE NAME TO YOUR APP PACKAGENAME TO OPEN UNITY APPLICATION
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("ahmednasser.scriba.autosar");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }
            }
        });

    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        current = location;
        if (start ==null){
            final LatLng dump = new LatLng(location.getLatitude(), location.getLongitude());
            start = dump;
            mMap.addMarker(new MarkerOptions().position(dump).title("Your Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(dump));
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    dist = latLng;
                    mMap.addMarker(new MarkerOptions().position(latLng).title("Distination"));
                    MakeQuery query = new MakeQuery(dump,latLng);
                    Log.v("query",query.makeQuery());
                    FetchUrl f = new FetchUrl(mMap,getApplicationContext());
                    f.execute(query.makeQuery());
                }
            });
        }else{
            if (dist!=null){
                LatLng c = new LatLng(current.getLatitude(), current.getLongitude());
                mMap.addMarker(new MarkerOptions().position(start).title("Your Location"));
                mMap.addMarker(new MarkerOptions().position(dist).title("Distination"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(c));

            }else{
                mMap.addMarker(new MarkerOptions().position(start).title("Your Location"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(start));
            }
        }


    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.v("check", mMap + "");

    }

}

