package ahmednasser.scriba.autosar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static androidx.core.app.ActivityCompat.requestPermissions;

/**
 * Created by ahmednasser on 3/16/18.
 */

public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
    private GoogleMap map;
    private Context con;

    public ParserTask(GoogleMap x,Context c) {
        map=x;
        con = c;
    }
//private PolylineOptions polyMap;
//private static IOnGetPathListener getListener ;

    public ParserTask(/*IOnGetPathListener clistner*/) {
       // getListener =clistner;
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
        Log.v("nasser","doinbackparser");
        JSONObject jObject;
        List<List<HashMap<String, String>>> routes = null;
        try {
            jObject = new JSONObject(jsonData[0]);
            DataParser parser = new DataParser();
            routes = parser.parse(jObject);
        } catch (Exception e) {
            Log.d("ParserTaskfailedparsing",e.toString());
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
        Log.v("nasser","onpostexeciteparser");
        ArrayList<LatLng> points;
        PolylineOptions lineOptions = null;
        String total =  "";

        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);
                String temp = String.valueOf(lat)+","+String.valueOf(lng)+","+"U"+"\n";
                total+=temp;
            }
            lineOptions.addAll(points);
            lineOptions.width(10);
            lineOptions.color(Color.RED);
        }
        if(lineOptions != null) {
           // polyMap=lineOptions;
            //TODO NASSER ON MAP
            map.addPolyline(lineOptions);
        }
        else {
            Log.d("onPostExecute","cant draw ");
        }

        writeFileOnInternalStorage(con,"autosarData",total);
    }

    public void writeFileOnInternalStorage(Context mcoContext, String sFileName, String sBody){
        Log.v("checkNasser", mcoContext.getFilesDir()+"");

        try {
            File testFile = new File(mcoContext.getExternalFilesDir(null), sFileName);
            if (!testFile.exists())
                testFile.createNewFile();

            BufferedWriter writer = new BufferedWriter(new FileWriter(testFile, true /*append*/));
            writer.write(sBody);
            writer.close();
            MediaScannerConnection.scanFile(mcoContext,
                    new String[]{testFile.toString()},
                    null,
                    null);
        } catch (IOException e) {
            Log.e("checkNasser", "Unable to write to file.");
            e.printStackTrace();
        }
    }

}